import React,{Component,Fragment} from 'react'
import styled from 'styled-components'
import cool from './icons/cool-emoji.png'
import boring from './icons/emoji-boring.jpg'
import smart from './icons/emoji-smart.png'
import PropTypes from 'prop-types'
import CommentModal from '../Comment/CommentModal'
import {openModal,toggleReactions} from './script'

const PostD = styled.div`
    background-color: #FFFFFF;
    padding: 10px;
    color: black;
    text-align:left;
    max-width:100%;
    margin: 10px 0px 10px 0px;
`
const PostHead = styled.div`
    postion:relative;
`
const HeadImage = styled.img`
    height:45px;
    width:45px;
    cursor:pointer;
`
const HeadText = styled.span`
    position:absolute;
    margin-top:5px;
`
const UserName = styled.span`
    position:relative;
    font-size:0.8em;
`
const UserInstitution = styled.span`
    position:relative;
    font-size:0.8em;
`
const PostBody = styled.div`

`
const PostText = styled.p`
    font-size:0.8em;
`
const PostReactions = styled.div`
    display: flow-root;
    margin: 3px 0px 0px 0px;
`
const ReactionCool = styled.img`
    height:30px;
    width: 30px;
    float:left;
    cursor:pointer;
`
const CoolValue = styled.div`
    background-color: #E5E5E5;
    border-radius:50%;
    width: 17px;
    height:12px;
    font-size: 10px;
    color:black;
    position:relative;
    top: 4ex;
    left:-4.7ex;
    text-align:center;
    float:left;
`
const ReactionBoring = styled.img`
    float:left;
    padding:0px 10px 0px 10px;
    height:30px;
    width: 30px;
    cursor:pointer;
`
const BoringValue = styled.div`
    background-color: #E5E5E5;
    border-radius:50%;
    width: 17px;
    height:12px;
    font-size: 10px;
    color:black;
    position:relative;
    top: 4ex;
    left:-6.5ex;
    text-align:center;
    float:left;
`
const ReactionSmart = styled.img`
    height:30px;
    width: 30px;
    padding:0px 0px 0px 9px;
    float:left;
    cursor:pointer;
`
const SmartValue = styled.div`
    background-color: #E5E5E5;
    border-radius:50%;
    width: 17px;
    height:12px;
    font-size: 10px;
    color:black;
    position:relative;
    top: 4ex;
    left:-4.5ex;
    text-align:center;
    float:left;
`
const RxnDropdown = styled.div`
    position:relative;
    display:none;
`
const RxnContent = styled.div`
    position:absolute;
    background-color: #C4C4C4;
    width:150px;
    z-index:1;
`
const RxnContentValue = styled.div`
    color: black;
    padding:3px 3px;
    margin: 2px 2px;
    text-decoration:none;
    display:block;
`
const RxnItem = styled.section`
    color:black;
    text-decoration:none;
    display:block;
    text-align:left;
    font-size: 12px;
    cursor:pointer;
    :hover{
        background-color: #E5E5E5;
    }
`
const RxnText = styled.a`
    text-decoration: none;
`
class Post extends Component{
    constructor(props){
        super(props)
        this.state = {
            cool: false,
            boring: false,
            smart: false
        }
        this.getReactions = this.getReactions.bind(this)
        this.closePostModal = this.closePostModal.bind(this)
    }
    getReactors(pid){
        let c = 0
        let r = this.props.reactors.find((v, i) => {
            c = v.allIds[i]
            return v.allIds[i] === pid
        })       
        return r[c]
    }
    async checkReacted(id){
        let postReactors = await this.getReactors(id)
        console.log(postReactors)
        let payload = {}
        for(let o of postReactors.rxtors){
            if(o.id === this.props.user.id){
                console.log("You have reacted")
                payload.res = true
                payload.o = o
                break
            }else{
                console.log("No you have not")
                payload.res = false
                payload.o = o
            }
        }
        return payload
    }
    async   updateReaction(id, rxn,n){
        let rxted = await this.checkReacted(id)
        console.log(rxted)
        let rxtorMutator = {
            pid: id,
            uid: this.props.user.id,
            rxn:rxn,
            add: false,
        }
        if (rxted.res === false){
            console.log("You have not reacted")            
            let serverData = {
                postid: id,
                rxn:rxn,
                action:true,
                target:n
            }
            rxtorMutator.add = true
            this.props.mutateRxtorLocally(rxtorMutator)
            var data = {
                id:id,
                rxn:rxn,
                stat: true
            }
            this.props.updateRxnLocally(data) // redux action
            this.props.updateServerRxn(serverData)
        }else if(rxted.res === true){
            // delete the user's rxtn
            var data = {
                id:id,
                rxn:rxn,
                rxn2:rxn,
                stat: false
            }
            if(rxted.o.rxn !== rxn){
                // NEW REACTION IS NOT EQUAL TO OLD REACTION
                data.rxn2 = rxted.o.rxn
                // CLEAN UP OLD REACTION - UNREACT TO POST
            }
            console.log("You have reacted")
            this.props.updateRxnLocally(data) // redux action
            //delete id locally from reactors
            rxtorMutator.add = false
            this.props.mutateRxtorLocally(rxtorMutator) // redux action
            let serverData = {
                postid: id,
                rxn:rxn,
                action: false,
                target:n,
            }
            this.props.updateServerRxn(serverData)
        }
    }
    getReactions(postId){
        let reactionsObj = {}
        let q = this.props.reactions.map((r,i)=>{
            let currentId = r.allIds[i]
            if(currentId === postId){
                reactionsObj = Object.assign({},r[currentId])
            }
        })
        return reactionsObj
    }
    openPostModals(postId,rxn){
        this.setState({
            [rxn]:true
        })
        //openModal(postId)
    }
    closePostModal(rxn){
        this.setState({
            [rxn]:false
        })
    }
    render(){
        let getReactionDropdown = (id,firstname,rxn,nick,lastname) => {
            return <RxnDropdown id={`${id}${firstname}${rxn}`}>
            <RxnContent>
                <RxnContentValue>
                    <RxnItem>
                        <RxnText onClick={()=>this.updateReaction(id,rxn,nick)}>
                            React ({rxn.charAt(0).toUpperCase() + rxn.slice(1)})
                        </RxnText>
                    </RxnItem>
                </RxnContentValue>
                <RxnContentValue>
                    <RxnItem>
                        <RxnText onClick={()=>this.openPostModals(`${id}${lastname}${rxn}`,rxn)}>
                            Comment
                        </RxnText>
                    </RxnItem>
                </RxnContentValue>
            </RxnContent>
        </RxnDropdown>
        }
        let renderCoolCommentModal = (id,modalid,classname,avatar,firstname,lastname,institution,comment,textInputId,rxn,postComment) => {
            if (!this.state.cool){
                return ''
            }else if(rxn !== 'cool'){
                return ''
            }else if(this.state.cool || rxn === 'cool'){
                return <CommentModal 
                id={id}
                modalid={modalid}
                classname={classname}
                avatar={avatar}
                firstname={firstname}
                lastname={lastname}
                institution={institution}
                comment={comment}
                textInputId={textInputId}
                rxn={rxn}
                postComment={postComment}
                closePostModal={this.closePostModal}
            />
            }else{
                return ''
            }
        }
        let renderBoringCommentModal = (id,modalid,classname,avatar,firstname,lastname,institution,comment,textInputId,rxn,postComment) => {
            if (!this.state.boring){
                return ''
            }else if(rxn !== 'boring'){
                return ''
            }else if(this.state.boring || rxn === 'boring'){
                return <CommentModal 
                id={id}
                modalid={modalid}
                classname={classname}
                avatar={avatar}
                firstname={firstname}
                lastname={lastname}
                institution={institution}
                comment={comment}
                textInputId={textInputId}
                rxn={rxn}
                postComment={postComment}
                closePostModal={this.closePostModal}
            />
            }else{
                return ''
            }
        }
        let renderSmartCommentModal = (id,modalid,classname,avatar,firstname,lastname,institution,comment,textInputId,rxn,postComment) => {
            if (!this.state.smart){
                return ''
            }else if(rxn !== 'smart'){
                return ''
            }else if(this.state.smart || rxn === 'smart'){
                return <CommentModal 
                id={id}
                modalid={modalid}
                classname={classname}
                avatar={avatar}
                firstname={firstname}
                lastname={lastname}
                institution={institution}
                comment={comment}
                textInputId={textInputId}
                rxn={rxn}
                postComment={postComment}
                closePostModal={this.closePostModal}
            />
            }else{
                return ''
            }
        }
        return(
            <PostD key={this.props.id}>
                <PostHead>
                    <HeadImage src={this.props.uimage}/>
                    <HeadText>
                        <UserName>{`${this.props.firstname} ${this.props.lastname}`}</UserName><br/>
                        <UserInstitution>{this.props.institution}</UserInstitution><br/>
                    </HeadText>
                </PostHead>
                <PostBody>
                    <PostText>
                        {this.props.text}
                    </PostText>
                </PostBody>
                <PostReactions>
                    <ReactionCool src={cool} onClick={()=> {toggleReactions(`${this.props.id}${this.props.firstname}cool`)}}/>
                    <CoolValue>{this.getReactions(this.props.id).cool}</CoolValue>
                    {getReactionDropdown(this.props.id, this.props.firstname,'cool',this.props.nick,this.props.lastname)}
                    {/* renderPostCommentModal conditionally */}
                    {renderCoolCommentModal(this.props.id,`${this.props.id}${this.props.lastname}cool`,'modal-content1',this.props.uimage,this.props.firstname,this.props.lastname,this.props.institution,this.props.text,'newcomment','cool',this.props.postComment)}
                    <ReactionBoring src={boring} onClick={()=> {toggleReactions(`${this.props.id}${this.props.firstname}boring`)}}/>
                    <BoringValue>{this.getReactions(this.props.id).boring}</BoringValue>
                    {getReactionDropdown(this.props.id, this.props.firstname,'boring',this.props.nick,this.props.lastname)}
                    {/* renderPostCommentModal conditionally */}
                    {renderBoringCommentModal(this.props.id,`${this.props.id}${this.props.lastname}boring`,'modal-content2',this.props.uimage,this.props.firstname,this.props.lastname,this.props.institution,this.props.text,'newcomment','boring',this.props.postComment)}
                    <ReactionSmart src={smart} onClick={()=> {toggleReactions(`${this.props.id}${this.props.firstname}smart`)}}/>
                    <SmartValue>{this.getReactions(this.props.id).smart}</SmartValue>
                    {getReactionDropdown(this.props.id, this.props.firstname,'smart',this.props.nick,this.props.lastname)}
                    {/* renderPostCommentModal conditionally */}
                    {renderSmartCommentModal(this.props.id,`${this.props.id}${this.props.lastname}smart`,'modal-content3',this.props.uimage,this.props.firstname,this.props.lastname,this.props.institution,this.props.text,'newcomment','smart',this.props.postComment)}
                </PostReactions>
            </PostD>
        );
    }
}

Post.propType = {
    id: PropTypes.string.isRequired,
    uimage: PropTypes.string.isRequired,
    firstname: PropTypes.string.isRequired,
    lastname: PropTypes.string.isRequired,
    institution: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    reactions: PropTypes.array.isRequired,
    reactors: PropTypes.string.isRequired,
    user: PropTypes.object.isRequired,
    updateRxnLocally: PropTypes.func.isRequired,
    mutateRxtorLocally: PropTypes.func.isRequired,
    updateServerRxn: PropTypes.func.isRequired,
    postComment: PropTypes.func.isRequired
}

export default Post;
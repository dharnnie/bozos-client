import styled from 'styled-components'

const Modal = styled.div`
    display:none;
    position:fixed;
    z-index:1;
    left:0;
    right:0;
    top:0;
    width:100%;
    height:100%;
    overflow:auto;
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4);
`
const ModalContent = styled.div`
    background-color: white;
    margin:5% 25% 20% 25%;
    padding:20px;
    border: 1px solid #888;
    width:40%;
    height:auto;
    //transition: top 500ms;
`
const ModalTextInput = styled.textarea`
    width:90%;
    height:70%;
    margin: 0px 0px 0px 5px;
    overflow:hidden;
    resize:none;
`
const ModalFoot = styled.div`
    display:flex;
    margin: 0px 0px 0px 45px;
`
const ModalFootItem = styled.div`
    flex:1;
`
const CloseBtn = styled.span`
    color: #aaa;
    float: right;
    margin:20px 20px 0px 0px;
    font-size: 28px;
    font-weight: bold;
    cursor: pointer;
`

export {Modal,ModalContent,ModalFoot,ModalFootItem,ModalTextInput,CloseBtn};
import {ALUM,ALUR,ALA,UNILAG,AUB} from '../Auth/script'
import {xALUM,xALUR,xALA,xUNILAG,xAUB} from '../Auth/script'

function openModal(id){
    var modal = document.getElementById(id)
    modal.style.display = 'block'
}
function closeModal(id){
    var modal = document.getElementById(id)
    modal.style.display = 'none'
}
function toggleReactions(id){
    var notifications = document.getElementById(id)
    let cstate = notifications.style.display
    if (cstate === 'none'){
        notifications.style.display = 'inline-block'
    }else{
        notifications.style.display = 'none'
    }
}
function closeCreateModal(id){
    let closeBtn = document.getElementById('close-create-modal')
    
}
// parse school for dashboard 
function parseSchool(school){
    switch(school){
        case ALUM:
            return xALUM
        case ALUR:
            return xALUR
        case ALA:
            return xALA
        case UNILAG:
            return xUNILAG 
        case UNILAG:
            return xUNILAG 
        case AUB:
            return xAUB
    }
}
export {openModal, closeModal,parseSchool, toggleReactions};
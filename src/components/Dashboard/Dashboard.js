import React,{Component,Fragment} from 'react'
import Wrapper from '../Wrapper/Wrapper'
import HeaderC from '../../containers/HeaderC'
import styled from 'styled-components'
import{Modal, ModalContent, ModalFoot,ModalFootItem,ModalTextInput,CloseBtn} from '../modals'
import {openModal,closeModal,parseSchool} from './script'
import Post from '../Post/Post'

const Button = styled.button`
    background-color: #C4C4C4;
    border: none; 
    font-size: 12px;
    width: 90px;
    height:32px;
    cursor: pointer;
`
const CreateBtn = styled(Button)`
    :hover{
        background-color: #E5E5E5;
    }
`
const DashWrap = styled.div`
    margin:18px 0px 0px 0px;
    display:flex;
`
const Left = styled.section`
    color: black;
    text-align:left;
    flex:2
    max-width: 57%;
`
const CreateWrapper = styled.div`
    max-width:100%;
    position:relative;
`
const Create = styled.div`
    background-color: #FFFFFF;
    padding-left: 5px;
    padding-bottom: 0px;
    color: black;
    text-align:left;
    max-width:100%;
    position:relative;
`
const UserImage = styled.img`
    height:40px;
    width:40px;
    position:relative;
`
const CreateTxtBtn = styled.span`
    display:inline-block;
    position:relative;
`
const Text = styled.span`
    font-size:0.8em;
    left:3ex;
    bottom: 1.5ex;
    position:relative;
`

const CreateBtnOne = styled(CreateBtn)`
    position:relative;
    left:35ex;
    bottom:2ex;
`
const CommentBtn = styled(CreateBtn)`

`
const BtnIcon = styled.i`
    color:#37474F;
`
const Right = styled.section`
    flex:1;
    margin-left: 15px;
`
const JustJoinedBox = styled.div`
    height: 180px;
    background-color: #FFFFFF;
    font-size: 15px;
    padding:20px 0px 0px 0px;
    position:sticky;
    top:60px;
    z-index:-2;
`
const JustJoinedHolder = styled.div`
    position:relative;
    margin-top:20px;
    padding-left:70px;
`
const JustJoinedImg = styled.div`
    float:left;
`
const JJImage = styled.img`
    width:110px;
    height:110px;
`
const JustJoinedInfo = styled.div`
    text-align:left;
    font-size:12px;
    padding-top:12px;
    margin-left:150px;
`
const DailyStuff = styled.div`
    height: 205px;
    background-color: #FFFFFF;
    font-size: 13px;
    padding:0px 0px 0px 0px;
    margin-top: 20px;
    text-align:left;
    position:sticky;
    top:280px;
`
const DailyStuffHeader = styled.div`
    height: 30px;
    background-color:#C4C4C4;
    padding-top:10px;
    text-align:center;
`
const DailyStuffText = styled.p`
    font-size:1em;
    margin:15px;
`
const BozoOfTheDay = styled.div`
    background-color: #FFFFFF;
    font-size: 15px;
    padding:0px 0px 0px 0px;
    margin-top: 20px;
    text-align:left;
    position:sticky;
    top:280px;
    display:flow-root;
`
const BozoOfTheDayImage = styled.div`
    margin: 10px 5px 0px 5px;
    float:left;
`
const BozoImg = styled.img`
    height:70px;
    width:70px;
`
const BozoStory = styled.p`
    margin: 10px 5px 5px 5px;
    font-size: 13px;
`
const ReadMoreBtn = styled.button`
    background-color: rgb(255, 0, 0, 0);
    //background-color: #C4C4C4;
    //background-color:rgb(0.77, 0.77, 0.77, 0.5);
    border: none; 
    margin:0px 0px 0px 5px;
    font-size: 12px;
    width: 90px;
    height:32px;
    cursor: pointer;
    margin-right: 5px;
    &:hover{
        background-color: #C4C4C4;
    }
`
const DModal = styled(Modal)`
    display:block;
`
const DModalContent = ModalContent
const DModalTextInput = ModalTextInput

const DModalFoot = ModalFoot
const DModalFootItem = ModalFootItem
const NotificationsModal = styled(Modal)`
    
`
const NotificationsContent = styled(ModalContent)`
    background-color: white;
    margin:4% 0% 20% 70%;
    padding:10px;
    border: 1px solid #888;
    width:27%;
    height:auto;
`
const NotificationItem = styled.section`
    color:black;
    text-decoration:none;
    display:block;
    text-align:left;
    font-size: 12px;
`
const NotificationText = styled.a`
    text-decoration: none;
    padding-left: 5px;
`
const NotificationImage = styled(UserImage)`
    height:21px;
    width:21px;
` 
const CloseModal = styled(CloseBtn)`
`
class Dashboard extends Component{
    constructor(props){
        super(props)
        this.state = {
            user: {},
            newPost:{
                text: ""
            },
            loading: false,
            sharebtn: 'Share',
            renderCModal: false
        }
        this.getUser = this.getUser.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.getShareIconClassName = this.getShareIconClassName.bind(this)
        this.toggleCreateModal = this.toggleCreateModal.bind(this)
    }
    componentWillMount(){
        this.getUser()
    }
    getUser(){
        let user = JSON.parse(localStorage.getItem('user')) 
        this.setState({user:user})
    }
    handleChange = (e) => {
        let newPost = Object.assign({},this.state.newPost)
        newPost.text = e.target.value
        this.setState({newPost})
    }
    sharePost(){
        this.setState({
            loading: true,
            sharebtn: 'Sending...'
        })
        let data = {
            id: 1,
            text: this.state.newPost.text,
            media:"",
            reactions:{
                cool:0,
                boring:0,
                smart:0
            },
            posttype:""
        }
        this.props.cp(data)
    }
    getShareIconClassName(){
        let loading = this.state.loading ? 'fa fa-spinner fa-spin' : 'fa fa-pencil'
        return loading
    }
    toggleCreateModal(toggleState){
        this.setState({
            renderCModal: toggleState
        })
    }
    render(){
        let renderCreateModal = () => {
            if(this.state.renderCModal){
                return <DModal id='create-modal'>
                <CloseModal id='close-create-modal'>&times;</CloseModal>
                <DModalContent className='modal-content'>
                    <UserImage src={this.state.user.avatar}/>
                    <DModalTextInput id='text' onChange={this.handleChange} value={this.state.newPost.text} placeholder='Remember, always spread value!'/>
                    <DModalFoot>
                        <DModalFootItem>
                            <CreateBtn style={{width:'60%',marginLeft:'0px'}} onClick={()=>this.toggleCreateModal(false)}>Close</CreateBtn>
                        </DModalFootItem>
                        <DModalFootItem>
                            <CreateBtn style={{width:'60%',marginLeft:'0px'}} onClick={()=>this.sharePost()}>
                                <BtnIcon style={{marginRight:'5px'}} className={this.getShareIconClassName()}/>
                                {this.state.sharebtn}
                            </CreateBtn>
                        </DModalFootItem>
                    </DModalFoot>
                </DModalContent>
            </DModal>
            }else{
                return ''
            }
        }
        let posts = this.props.posts.map((post, index) => {
            let postId = post.allIds[index]
            return <Post
                key={post[postId].id}
                id={post[postId].id}
                uimage={post[postId].avatar}
                firstname={post[postId].firstname}
                lastname={post[postId].lastname}
                institution={parseSchool(post[postId].institution)}
                text={post[postId].text}
                user={this.state.user}
                reactions={this.props.reactions}
                reactors={this.props.reactors}
                mutateRxtorLocally={this.props.mutateRxtorLocally}
                updateRxnLocally={this.props.updateRxnLocally}
                updateServerRxn={this.props.updateServerRxn}
                mutateServerRxtor={this.props.mutateServerRxtor}
                postComment={this.props.postComment}
            />
        });
        let w = (
            <DailyStuff>
                <DailyStuffHeader>
                    {this.props.wodd.word} | Word of the day
                </DailyStuffHeader>
                <DailyStuffText>
                    {this.props.wodd.quote}
                </DailyStuffText>
            </DailyStuff>
        );
        return(
            <Fragment>
                <NotificationsModal className='notifications'>
                    <NotificationsContent>
                        <NotificationItem>
                            <NotificationImage src={'https://res.cloudinary.com/bootable/image/upload/v1537700629/Daniel_Osineye_rvvle1.jpg'}/>
                            <NotificationText href={'https://www.facebook.com/'}>Daniel thinks your post is cool (He mentioned why)</NotificationText> 
                        </NotificationItem><hr/>
                        <NotificationItem>
                            <NotificationImage src={'https://res.cloudinary.com/bootable/image/upload/v1537700629/Daniel_Osineye_rvvle1.jpg'}/>
                            <NotificationText>Roland mentioned why he thinks your post is boring</NotificationText>
                        </NotificationItem><hr/>
                        <NotificationItem>
                            <NotificationImage src={'https://res.cloudinary.com/bootable/image/upload/v1537700629/Daniel_Osineye_rvvle1.jpg'}/>
                            <NotificationText>Daniel thinks your post is smart</NotificationText>
                        </NotificationItem><hr/>
                    </NotificationsContent>
                </NotificationsModal>
                <HeaderC/>
                <Wrapper page='Feed'>
                    <DashWrap>
                        <Left>
                            <CreateWrapper>
                                <Create>
                                    <UserImage src={this.state.user.avatar} />
                                        <Text>Hey {this.state.user.firstname}, got something cool to share?</Text>
                                        <CreateBtnOne onClick={()=>this.toggleCreateModal(true)}>
                                            <BtnIcon className='fa fa-pencil'/>
                                        </CreateBtnOne>
                                    {renderCreateModal()}
                                </Create>
                            </CreateWrapper>
                            {/* {newPost} */}
                            {posts}
                        </Left>
                        <Right>
                            <JustJoinedBox className='justjoinedbox'>
                                Look who just joined
                                <JustJoinedHolder>
                                    <JustJoinedImg>
                                        <JJImage src={'https://res.cloudinary.com/bootable/image/upload/v1537700629/Daniel_Osineye_rvvle1.jpg'}/>
                                    </JustJoinedImg>
                                    <JustJoinedInfo>
                                        <p>Daniel Osineye</p>
                                        <p>American University of Beirut</p>
                                        <p>Lebanon</p>
                                    </JustJoinedInfo>
                                </JustJoinedHolder>
                            </JustJoinedBox>
                            {w}
                            <BozoOfTheDay>
                                <DailyStuffHeader>
                                    Samuel Adewole | BOZO of the day
                                </DailyStuffHeader>
                                <BozoOfTheDayImage>
                                    <BozoImg src={'https://res.cloudinary.com/bootable/image/upload/v1537700629/Daniel_Osineye_rvvle1.jpg'}/>
                                </BozoOfTheDayImage>
                                <BozoStory>
                                   Samuel studies Entreprenuership at ALU (The African Leadership University) Rwanda. His team (Volta Irrigation) was one of the Hult Prize finalists in Rwanda walking away with $30,000 USD in prize money with an opportunity to compete for $1m USD in London. Prior to ALU, Samuel was a Chemical Engineering student at The University of Lagos, Nigeria. Be inspired by Samuel's transition from UNILAG, to TBP to ALU and Volta Irrigation. 
                                </BozoStory>
                                <ReadMoreBtn onClick={()=> alert("You will be able to read more about Inspiring BOZOS soon...")}>
                                    <BtnIcon  style={{marginRight:'3px'}} className='fa fa-book'/> 
                                    Read more
                                </ReadMoreBtn>
                                {/* <BODDModal id='bodd-modal'>
                                    <BODDContent>
                                        <BODDHeader>
                                            Header
                                        </BODDHeader>
                                    </BODDContent>
                                </BODDModal> */}
                            </BozoOfTheDay>
                        </Right>
                    </DashWrap>
                </Wrapper>
            </Fragment>
        )
    }
}

export default Dashboard
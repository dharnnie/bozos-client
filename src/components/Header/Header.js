import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import styled from 'styled-components'
import {parseInstitutions} from './script'
import bell from '../Header/icons/bell-regular.svg'
import { toggleNotifications } from './script';

const SHeader = styled.header`
  background-color: #37474F;
  height: 15px;
  padding: 20px;
  align-content: center;
  color: white;
  position:sticky;
  top:0;
  z-index:2;
`
const Logo = styled.div`
    position:absolute;
    left: 2%;
    cursor:pointer;
    color:white;
`
const User = styled.div`
    position: relative;
    // left: 60%;
    // top:20%;
    cursor:pointer;
    color:white;
`
const UserAvatar = styled.img`
    position: absolute;
    height:28px;
    width:28px;
    left: 58%;
    // top: 0%;
    // bottom: 0%;
`
const UserInfo = styled.div`
    position:absolute;
    padding-left:3em;
    display:block;
    font-size:12px;
    left: 29.57%;
    right: 5%;
    top: 0%;
    bottom: 52%;
`
const UserName = styled.span`
    display:block;
    width:auto;
`
const UserSchool = styled.span`
    width:auto;
`
const HeaderItems  = styled.ul`
    display:relative;
    position: absolute;
    left: 70%;
    align:center;
    top: 0px;
`
const ItemNotifications = styled.li`
    list-style:none;
    position:relative;
`
const NotificationsIcon = styled.span`
    position:absolute;
    height:24px;
    width:24px;
    color: #c7d1d8;
    cursor:pointer;
    text-align:center;
`
const Icon = styled.img`
    height:28px;
    width:28px;
`
const IconValue = styled.div`
    position:relative;
    left:27px;
    top:8px;
    background-color: #E5E5E5;
    border-radius:50%;
    border:1px solid white;
    width: 15px;
    height:8.5px;
    padding:1px;
    font-size: 10px;
    color:black;
`
const UserImage = styled.img`
    float:left;
`
const NotificationDropdown = styled.div`
    position:relative;
    display:none;
`
const DropdownContent = styled.div`
    position:absolute;
    background-color: #C4C4C4;
    width:200px;
    z-index:1;
`
const DropdownContentValue = styled.div`
    color: black;
    padding:3px 3px;
    margin: 2px 2px;
    text-decoration:none;
    display:block;
`

const NotificationItem = styled.section`
    color:black;
    text-decoration:none;
    display:block;
    text-align:left;
    font-size: 12px;
    cursor:pointer;
`
const NotificationText = styled.a`
    text-decoration: none;
`
const NotificationImage = styled(UserImage)`
    height:21px;
    width:21px;         
    margin-bottom: 3px;
`

class Header extends Component {
    constructor(props){
        super(props)
        this.state = {
            user: ''
        }
        this.getUser = this.getUser.bind(this)
    }
    componentWillMount(){
        this.getUser()
    }
    getUser(){
        var user = JSON.parse(localStorage.getItem('user')) 
        let abbr = parseInstitutions(user.institution)
        user.institution = abbr
        this.setState({user:user})
    } 
    
    render(){
        let notifs = this.props.notifications.length === 0 ? 'No notifications':  this.props.notifications.map((n,i) => {
            return <DropdownContentValue key={i}>
                <NotificationItem>
                    <NotificationText>
                        {n.message}
                    </NotificationText><br/><hr/>
                </NotificationItem>
            </DropdownContentValue>
        })
        return(
            <SHeader>
                <Link to='/'>
                    <Logo>HEKMA</Logo>
                </Link>
                {/* Link this to profile page when it is ready */}
                <Link to='/'> 
                    <User>
                        <UserAvatar src={this.state.user.avatar}/>
                        <UserInfo>
                            <UserName>{this.state.user.firstname}</UserName>
                            <UserSchool>{this.state.user.institution}</UserSchool>
                        </UserInfo>
                    </User>
                </Link>
                <HeaderItems>
                    <ItemNotifications>
                        <NotificationsIcon onClick={()=> {toggleNotifications('notifications')}}>
                            <Icon src={bell}></Icon>
                        </NotificationsIcon>
                        <IconValue>{this.props.notifications.length}</IconValue>
                        <NotificationDropdown id='notifications'>
                            <DropdownContent>
                                {notifs}
                            </DropdownContent>
                        </NotificationDropdown>
                    </ItemNotifications>
                </HeaderItems>
            </SHeader>
        )
    }
}

export default Header
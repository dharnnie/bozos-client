import {ALUM,ALUR,ALA,AUB,UNILAG} from '../Auth/script'
import {xALUM,xALUR,xALA,xAUB,xUNILAG} from '../Auth/script'
function toggleNotifications(id){
    var notifications = document.getElementById(id)
    let cstate = notifications.style.display
    if (cstate === 'none'){
        notifications.style.display = 'inline-block'
    }else{
        notifications.style.display = 'none'
    }
}

function parseInstitutions(institutions){
    switch (institutions){
        case ALUM: 
            return xALUM
        case ALUR: 
            return xALUR
        case ALA: 
            return xALA
        case UNILAG: 
            return xUNILAG
        case AUB: 
            return xAUB
        default:
            return 'NoSchool?'
    }
}

export {parseInstitutions,toggleNotifications}
export const isEmail = (value) =>{
    let regEx = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regEx.test(value)){
        return false
    }else{
        return true
    }
}
// checks if id is nick or email format
export const checkSignUpPayload = (sPayload) =>{
    if (isEmail(sPayload.email)){
        //alert(isEmail(payload.email))
        let payload = {
            verStat: true,
            main:{
                kind:"student",
                email:sPayload.email,
                nick: sPayload.nick,
                firstname: sPayload.firstname,
                lastname:sPayload.lastname,
                password:sPayload.password,
                gender: sPayload.gender,
                institution: sPayload.institution
            }
        }
        return payload
    }else{
        let payload = {
            verStat: false,
            message: "Please use a valid email format [e.g daniel@bozos.xyz]"
        }
        return payload
    }
}

export const checkLoginPayload = (id,password) =>{
    if (isEmail(id)){
        var payload = {
            email:id,
            nick: '',
            password:password,
            type:'email'
        }
        return payload
    }else{
        var payload = {
            email:'',
            nick: id,
            password:password,
            type:'nick'
        }
        return payload
    } 
} 
import React,{Component} from 'react'
import styled from 'styled-components'
import {Redirect} from 'react-router-dom'
import {connect} from 'react-redux';
import { checkLoginPayload } from '.';
import { signIn, setSignedIn} from '../../redux/actions/authActions';


const Container = styled.div`
    display:grid;
    grid-template-columns: 50% 50%;
    grid-template-rows: 670px;
    @media (max-width: 700px){
        grid-template-columns: 50% 50%;
    }
    @media (max-width: 560px){
        grid-template-columns: 1fr;
    }
`;
const Left = styled.div`
    display:grid
    overflow:hidden;
    background-color:#37474F;  
`
const Right = styled.div`
    display:grid
    align-self:center;
    justify-self:center;
`
const AppName = styled.p`
    padding:5px;
    font-size:50px;
`
const Info = styled.p`
    padding:5px;
    font-size:20px;
    color:#C4C4C4;
`
const Form = styled.form`
    display:grid;
    grid-template-columns: 1fr;
    margin:10px;
`
const NickField = styled.input`
    padding:10px;
    margin:10px;
    @media (max-width: 670px){
        padding:4px;
        margin:4px;
    }
`
const PasswordField = styled.input`
    padding:10px;
    margin:10px;
    @media (max-width: 670px){
        padding:4px;
        margin:4px;
    }
`
const Btn = styled.button`
    padding:10px;
    margin:10px;
    @media (max-width: 670px){
        padding:4px;
        margin:4px;
    }
`

class Signin extends Component{
    constructor(props){
        super(props)
        this.state = {
            id: '',
            password: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id] : e.target.value
        })
    }
    async handleSubmit(e){
        e.preventDefault()
        let payload = await checkLoginPayload(this.state.id,this.state.password)
        await this.props.signIn(payload)
        if (this.props.signInRes.status === "success"){
            localStorage.setItem("token",this.props.signInRes.token)
            localStorage.setItem("signedIn",true)
            localStorage.setItem("user",JSON.stringify(this.props.signInRes.user))
            this.props.setSignedIn(true) // redux action
        }else{
            alert("failed: ", this.props.signInRes.status)
        }
    }
    render(){
        if (this.props.signedIn){
            return(
                <Redirect to='/'/>
            )
        }
        return(
            <Container>
                <Left></Left>
                <Right>
                    <AppName>BOZOS</AppName>
                    <Info>Being a BOZO is the new cool</Info>
                    <Form onSubmit={this.handleSubmit}>
                        <NickField id='id' onChange={this.handleChange} placeholder='Nick' defaultValue={this.state.id}></NickField>
                        <PasswordField id='password' onChange={this.handleChange} type='password' placeholder='Password' defaultValue={this.state.password}></PasswordField>
                        <Btn>Login</Btn>
                    </Form>
                </Right>
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    signedIn: state.auth.signedIn,
    signInRes: state.auth.signInRes,
    emailExists: state.auth.emailExists,
    nickExists: state.auth.nickExists,
})
export default connect(mapStateToProps,{
        signIn,
        setSignedIn,
    })(Signin);
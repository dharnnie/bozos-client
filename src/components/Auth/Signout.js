import React,{Component} from 'react'
import {connect} from 'react-redux';
import {Link} from 'react-router-dom'
import {setSignedIn} from '../../redux/actions/authActions'

class Signout extends Component{
    componentWillMount(){
        this.props.setSignedIn(false)
        localStorage.clear()
    }
    render(){
        return(
            <div>
                You are now signed out <br/><br/><br/>
                <Link to='/signin'>SignIn</Link>
                <br/><br/><br/>
                <Link to='/signup'>SignUp</Link>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    signedIn: state.auth.signedIn
})

export default connect(mapStateToProps,{
        setSignedIn,
    })(Signout);
// Values to display to users
export const ALUM = 'African Leadership College (Mauritius)'
export const ALUR = 'African Leadership University (Rwanda)'
export const ALA = 'African Leadership Academy (ALA)'
export const UNILAG = 'University of Lagos (UNILAG)'
export const AUB = 'American University of Beirut (Lebanon)'

// Stripped down values for Dashboard
export const xALUM = 'ALU(Mauritius)'
export const xALUR = 'ALU(Rwanda)'
export const xALA = 'ALA(South Africa)'
export const xUNILAG = 'UNILAG(Lagos)'
export const xAUB = 'AUB(Lebanon)'
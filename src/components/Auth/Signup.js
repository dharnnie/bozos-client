import styled from 'styled-components'
import React from 'react';
import {Redirect} from 'react-router-dom'
import {checkSignUpPayload} from './index'
import {connect} from 'react-redux';
import {ALUM,ALUR,ALA,UNILAG,AUB} from './script'
import {nExists, eExists, signUp, setSignedIn} from '../../redux/actions/authActions';


const Outer = styled.div`
    display:grid;
    grid-template-columns: 50% 50%;
    grid-template-rows: 670px;
    grid-gap:20px;
    @media (max-width: 700px){
        grid-template-columns: 50% 50%;
        grid-gap:20px;
    }
    @media (max-width: 560px){
        grid-template-columns: 1fr;
    }
`;

const Left = styled.div`
    display:grid
    overflow:hidden;
    background-color:#37474F;  
`
const Right = styled.div`
    display:grid
    align-self:center;
    justify-self:center;
    
`
const AppName = styled.p`
    padding:5px;
    font-size:50px;
`
const Info = styled.p`
    padding:5px;
    font-size:20px;
    color:#C4C4C4;
`
const RightInputs = styled.form`
    display:grid;
    grid-template-columns: 1fr 1fr;
    margin:10px;
    @media (max-width: 1100px){
        grid-template-columns: 1fr;
    }
`
const NickField = styled.input`
    padding:10px;
    margin:10px;
    @media (max-width: 670px){
        padding:4px;
        margin:4px;
    }
`
const FirstNameField = styled.input`
    padding:10px;
    margin:10px;
    @media (max-width: 670px){
        padding:4px;
        margin:4px;
    }
`
const LastNameField = styled.input`
    padding:10px;
    margin:10px;
    @media (max-width: 670px){
        padding:4px;
        margin:4px;
    }
`
const EmailField = styled.input`
    padding:10px;
    margin:10px;
    @media (max-width: 670px){
        padding:4px;
        margin:4px;
    }
`
const Institution = styled.select`
    padding:10px;
    margin:10px;
    @media (max-width: 670px){
        padding:4px;
        margin:4px;
    }
`
const Gender = styled(Institution)`
    
`
const PasswordField = styled.input`
    padding:10px;
    margin:10px;
    @media (max-width: 670px){
        padding:4px;
        margin:4px;
    }
`
const Btn = styled.button`
    padding:10px;
    margin:20px;
    grid-column:1 / 2;
    @media (max-width: 670px){
        padding:4px;
        margin:4px;
        margin:15px;
    }
`
class Signup extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            nick: '',
            email: '',
            firstname: '',
            lastname: '',
            institution: '',
            gender:'',
            password: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id] : e.target.value
        })
    }
    async handleSubmit(e){
        e.preventDefault()
        let arr = Object.entries(this.state)
        for (const [key, value] of arr){
            if (value.length < 1){
                alert(`${key} field is empty`)
            }
        }
        let p = this.state
        let verPayload = await checkSignUpPayload(p)
        if (!verPayload.verStat){
            alert(`${this.state.email} is not a valid email address`)
        }else{
            await this.register(verPayload.main)
            if(this.props.signUpRes.status === 'success'){
                localStorage.setItem('token', this.props.signUpRes.token)
                localStorage.setItem('signedIn', true)
                localStorage.setItem('user',JSON.stringify(this.props.signUpRes.user))
                this.props.setSignedIn(true) // redux action
            }else{
                console.log(`Sign up status - ${this.props.signUpRes.status}`)
            }
        }
    }
    async register(data){
        await this.props.signUp(data)
    }
    render(){
        if (this.props.signedIn){
            return(
                <Redirect to='/'/>
            )
        }
        return(
            <Outer>
                <Left></Left>
                <Right>
                    <AppName>BOZOS</AppName>
                    <Info>Being a BOZO is the new cool!</Info>
                    {/* <Info>  </Info> */}
                    <RightInputs onSubmit={this.handleSubmit}>
                        <NickField id='nick' onChange={this.handleChange} defaultValue={this.state.nick} placeholder='Nick'></NickField>
                        <EmailField id='email' onChange={this.handleChange} defaultValue={this.state.email} placeholder='Email'></EmailField>
                        <FirstNameField id='firstname' onChange={this.handleChange} defaultValue={this.state.firstname} placeholder='Firstname'></FirstNameField>
                        <LastNameField id='lastname' onChange={this.handleChange} defaultValue={this.state.lastname} placeholder='Lastname'></LastNameField>
                        <Institution id='institution' onChange={this.handleChange} defaultValue={this.state.institution} placeholder='Institution'>
                            <option value>Institution</option>
                            <option>{ALUM}</option>
                            <option>{ALUR}</option>
                            <option>{ALA}</option>
                            <option>{UNILAG}</option>
                            <option>{AUB}</option>
                        </Institution>
                        <Gender id='gender' onChange={this.handleChange} defaultValue={this.state.gender} placeholder='Gender'>
                            <option value>Gender</option>
                            <option>Female</option>
                            <option>Male</option>
                        </Gender>
                        <PasswordField id='password' onChange={this.handleChange} defaultValue={this.state.password} type='password' placeholder='Password'></PasswordField>
                        <Btn>Signup</Btn>
                    </RightInputs>
                    {/* <Btn>Signup</Btn> */}
                </Right>
            </Outer>
        );
    }
}
const mapStateToProps = state => ({
    signedIn: state.auth.signedIn,
    signInRes: state.auth.signInRes,
    signUpRes: state.auth.signUpRes,
    emailExists: state.auth.emailExists,
    nickExists: state.auth.nickExists,
})
export default connect(mapStateToProps,{
        eExists,
        nExists,
        signUp,
        setSignedIn,
    })(Signup);
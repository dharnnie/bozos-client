import React,{Component,Fragment} from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import{Modal, ModalContent, ModalFoot,ModalFootItem,ModalTextInput,CloseBtn} from '../modals'
import{fetchCommentsById} from '../../redux/actions/feedActions'
import Comment from '../Comment/Comment'
import {connect} from 'react-redux';

const ItemInfo = styled.span`
    color:#37474F
    font-size: 65%;
`
const CModal = styled(Modal)`
    display: block;
    z-index: 3;
`
const ItemHead = styled.div`
    margin-top: 5px;
    font-size: 90%;
`
const ItemText = styled.p`
    font-size:80%;
`
const HeadImage = styled.img`
    height:45px;
    width:45px;
    right:-1ex;
    top:-0.5ex;
    cursor:pointer;
`
const UserName = styled.span`
    position:relative;
    top:-3.4ex;
    left:1ex;
    font-size:0.8em;
`
const UserInstitution = styled.span`
    position:relative;
    top:-1ex;
    right:12ex;
    font-size:0.8em;
`
const CommentsTextInput = styled(ModalTextInput)`
    width:100%;
    height:70%;
    margin: 0px 5px 0px 0px;
`
const Button = styled.button`
    background-color: #C4C4C4;
    border: none; 
    margin:4px 0px 0px 200px;
    font-size: 12px;
    width: 90px;
    height:32px;
    cursor: pointer;
    // &:hover{
    //     background-color: #C4C4C4;
    // }
`
const CloseModal = styled(CloseBtn)`

`
class CommentModal extends Component{
    constructor(props){
        super(props)
        this.state = {
            newComment:{
                comment: ""
            }
        }
        this.fetchComments = this.fetchComments.bind(this)
        this.filterCommentsByIdAndRxn = this.filterCommentsByIdAndRxn.bind(this)
    }
    componentWillMount(){
        let token = localStorage.getItem('token')
        this.fetchComments(token,this.props.id)
    }
    async fetchComments(token,pid){
        await fetchCommentsById(token,pid)
    }
    filterCommentsByIdAndRxn(){
        let c = []
        let copy = JSON.parse(JSON.stringify(this.props.comments))
        copy.forEach((item) => {
            if(item.postid === this.props.id && item.category === this.props.rxn){
                c.push(item)
            }
        })
        return c
    }
    createComment = (e)=> {
        let newComment = Object.assign({}, this.state.newcomment)
        newComment.comment = e.target.value
        this.setState({newComment})
    }
    async postComment(postid,category){
        console.log(postid)
        let payload = {
            postid:postid,
            comment: this.state.newComment.comment,
            category: category
        }
        this.props.postComment(payload)
    }
    render(){
        let comments = this.filterCommentsByIdAndRxn()
        let c = comments.reverse()
        let getComments = c.map((item) => { 
            return <Comment 
                key={item.commentsid}
                id={item.commentsid}
                uimage={item.who.avatar}
                firstname={item.who.firstname}
                lastname={item.who.lastname}
                institution={item.who.institution}
                text={item.comment}
            />
        })
        return(
            <Fragment>
                <CModal id={this.props.modalid}>
                    <CloseModal onClick={() => this.props.closePostModal(this.props.rxn)}>&times;</CloseModal>
                    <ModalContent className={this.props.classname}>
                        <ItemInfo>Let {this.props.firstname} know why you think this is {this.props.rxn}</ItemInfo>
                        <ItemHead>
                            <HeadImage src={this.props.avatar}/>
                            <UserName>{`${this.props.firstname} ${this.props.lastname}`}</UserName>
                            <UserInstitution>{this.props.institution}</UserInstitution>
                        </ItemHead>
                        <ItemText>
                            {this.props.comment}
                        </ItemText>
                        <ItemInfo>Leave a comment</ItemInfo>
                        <CommentsTextInput id={this.props.textInputId} value={this.state.newcomment} onChange={this.createComment} placeholder='Remember, always spread value!'/>
                        <ModalFootItem>
                            <Button style={{width:'20%',marginLeft:'0px'}} onClick={()=>this.postComment(this.props.id,this.props.rxn)}>Share</Button>
                        </ModalFootItem>
                        {getComments}
                    </ModalContent>
                </CModal>
            </Fragment>
        )
    }
}

CommentModal.propTypes = {
    id: PropTypes.number.isRequired,
    classname: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    firstname: PropTypes.string.isRequired,
    lastname: PropTypes.string.isRequired,
    institution: PropTypes.string.isRequired,
    comment: PropTypes.string.isRequired,
    textInputId: PropTypes.string.isRequired,
    rxn: PropTypes.string.isRequired,
    postComment: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    comments: state.feed.comments
})
export default connect(mapStateToProps)(CommentModal);
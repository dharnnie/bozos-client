import React,{Component,Fragment} from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const PostD = styled.div`
    background-color: #FFFFFF;
    padding: 10px;
    color: black;
    text-align:left;
    max-width:100%;
    margin: 10px 0px 10px 0px;
`
const PostHead = styled.div`

`
const HeadImage = styled.img`
    height:45px;
    width:45px;
    right:-1ex;
    top:-0.5ex;
    cursor:pointer;
`
const UserName = styled.span`
    position:relative;
    top:-3.4ex;
    left:1ex;
    font-size:0.8em;
`
const UserInstitution = styled.span`
    position:relative;
    top:-1ex;
    right:12ex;
    font-size:0.8em;
`
const PostBody = styled.div`

`
const PostText = styled.p`
    font-size:0.8em;
`


class Comment extends Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <PostD key={this.props.id}>
                <PostHead>
                    <HeadImage src={this.props.uimage}/>
                    <UserName>{`${this.props.firstname} ${this.props.lastname}`}</UserName>
                    <UserInstitution>{this.props.institution}</UserInstitution>
                </PostHead>
                <PostBody>
                    <PostText>
                        {this.props.text}
                    </PostText>
                </PostBody>
            </PostD>
        );
    }
}

Comment.propType = {
    id: PropTypes.number.isRequired,
    uimage: PropTypes.string.isRequired,
    firstname: PropTypes.string.isRequired,
    lastname: PropTypes.string.isRequired,
    institution: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired
}

export default Comment;
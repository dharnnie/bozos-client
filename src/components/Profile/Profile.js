import React,{Component, Fragment} from 'react'
import styled from 'styled-components'
import Header from '../Header/Header'
import Wrapper from '../Wrapper/Wrapper'
import {openItemModal} from './script'
import cool from '../Dashboard/icons/cool-emoji.png'
import boring from '../Dashboard/icons/emoji-boring.jpg'
import smart from '../Dashboard/icons/emoji-smart.png'
import tino from '../Dashboard/media/tinos2.jpg'

const ProfileWrap = styled.div`
    margin:18px 0px 0px 0px
    display:grid;
    grid-template-columns: 1fr;
`
const ProfileHead = styled.div`
    background-color: #37474F;
    padding: 15px;
    color: white;
    height: 225px;
    font-size:13px;
`
const ProfileHeadContent = styled.div`
    display:grid;
    grid-template-column:1fr 1fr;
    grid-template-rows:15px auto 15px;
    padding:15px;
`
const ProfileHeadUsername = styled.div`
    grid-column:1 / 3
    grid-row: 1;
    text-align:left;
    margin-left:1.5em;
`
const ProfileHeadContentMiddle = styled.div`
    display:flex;
`

const ProfileHeadContentMiddleLeft = styled.div`
    flex: 1;
`
const ProfileHeadImage = styled.img`
    height: 150px;
    margin-right:8px;
    //width: 25%;
    width: 135px;
    float:left;
`
const ProfileHeadContentMiddleRight = styled.div`
    flex: 1;
`
const ProfileHeadInfo = styled.p`
    text-align:left;
    margin-block-start: 0em;
    margin-block-end: 1em;
`
const ProfileHeadInfoNick = styled(ProfileHeadInfo)`

`
const ProfileHeadLocation = styled(ProfileHeadInfo)`

`
const ProfileHeadInstitution = styled(ProfileHeadInfo)`

`
const ProfileHeadTitle = styled(ProfileHeadUsername)`
    grid-row: 3;
    text-align:left;
`
const ProfileBody = styled.div`
    background-color:white;
    padding:15px;
    margin-top:15px;
    margin-right:30px;
    margin-left:30px;
    font-size 13px;
    text-align:left;
`
const ItemInfo = styled.span`
    color:#37474F
    font-size: 85%;
`
const ItemHead = styled.div`
    margin-top: 5px;
`
const HeadImage = styled.img`
    height:35px;
    width:35px;
    right:-1ex;
    top:-0.5ex;
    cursor:pointer;
`
const UserName = styled.span`
    position:relative;
    top:-3.4ex;
    right:-1ex;
    font-size: 90%;
`
const UserInstitution = styled.span`
    position:relative;
    top:-1ex;
    right:12.2ex;
    font-size: 90%;
`
const ItemText = styled.p`
    font-size:90%;
`
const CheckOutBtn = styled.button`
    background-color:#C4C4C4
    border: none; 
    margin:0px 0px 0px 0px;
    font-size: 12px;
    width: 90px;
    height:32px;
    cursor: pointer;
    margin-right: 5px;
    // &:hover{
    //     background-color: #C4C4C4;
    // }
`
const BtnIcon = styled.i`
    color:#37474F;
`
const ItemModal = styled.div`
    display:none;
    position:fixed;
    z-index:1;
    left:0;
    right:0;
    top:0;
    width:100%;
    height:100%;
    overflow:auto;
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4);
`
const ModalContent = styled.div`
    background-color: white;
    margin:5% 25% 20% 25%;
    padding:20px;
    border: 1px solid #888;
    width:40%;
    height:auto;
    //transition: top 500ms;
`
const PostImage = styled.img`
    max-width: 100%;
`
const PostReactions = styled.div`
    display: flow-root;
    margin: 3px 0px 0px 0px;
`
const ReactionCool = styled.img`
    height:30px;
    width: 30px;
    float:left;
    cursor:pointer;
`
const CoolValue = styled.div`
    background-color: #E5E5E5;
    border-radius:50%;
    width: 17px;
    height:12px;
    font-size: 10px;
    color:black;
    position:relative;
    top: 4ex;
    left:-4.7ex;
    text-align:center;
    float:left;
`
const ReactionBoring = styled.img`
    float:left;
    padding:0px 10px 0px 10px;
    height:30px;
    width: 30px;
    cursor:pointer;
`
const BoringValue = styled.div`
    background-color: #E5E5E5;
    border-radius:50%;
    width: 17px;
    height:12px;
    font-size: 10px;
    color:black;
    position:relative;
    top: 4ex;
    left:-6.5ex;
    text-align:center;
    float:left;
`
const ReactionSmart = styled.img`
    height:30px;
    width: 30px;
    padding:0px 0px 0px 9px;
    float:left;
    cursor:pointer;
`
const SmartValue = styled.div`
    background-color: #E5E5E5;
    border-radius:50%;
    width: 17px;
    height:12px;
    font-size: 10px;
    color:black;
    position:relative;
    top: 4ex;
    left:-4.5ex;
    text-align:center;
    float:left;
`
class Profile extends Component{
    render(){
        return(
            <Fragment>
                <Header/>
                <Wrapper page='Profile'>
                    <ProfileWrap>
                        <ProfileHead>
                            <ProfileHeadContent>
                                <ProfileHeadUsername>
                                    Daniel Osineye
                                </ProfileHeadUsername>
                                <ProfileHeadContentMiddle>
                                    <ProfileHeadContentMiddleLeft>
                                        <ProfileHeadImage src={'https://res.cloudinary.com/bootable/image/upload/v1537700629/Daniel_Osineye_rvvle1.jpg'}/>
                                        <ProfileHeadInfo>I love nerds. I crave Innovation. Thrive to get better by the day. I'm excited about robotics. I believe Christ is a nerd. iCode(). Intern @ALAcademy</ProfileHeadInfo>
                                        <ProfileHeadInfoNick>@dharnnie</ProfileHeadInfoNick>
                                        <ProfileHeadLocation>Lagos, Nigeria</ProfileHeadLocation>
                                        <ProfileHeadInstitution>African Leadership University</ProfileHeadInstitution>
                                    </ProfileHeadContentMiddleLeft>
                                    <ProfileHeadContentMiddleRight></ProfileHeadContentMiddleRight>
                                </ProfileHeadContentMiddle>
                                <ProfileHeadTitle>
                                    Intern (GoogleX)
                                </ProfileHeadTitle>
                            </ProfileHeadContent>
                        </ProfileHead>
                        <ProfileBody>
                            <ItemInfo>Daniel thinks this is cool</ItemInfo>
                            <ItemHead>
                                <HeadImage src={'https://res.cloudinary.com/bootable/image/upload/v1537700629/Daniel_Osineye_rvvle1.jpg'}/>
                                <UserName>Daniel Osineye</UserName>
                                <UserInstitution>African Leadership University</UserInstitution>
                            </ItemHead> 
                            <ItemText>
                                Ideas are never fully formed. You just need to keep working on them. When we started, it was like a joke, we were not sure how this would turn out. We only gave it a shot and it paid off.
                            </ItemText>
                            <CheckOutBtn onClick={()=>openItemModal('item-modal')}>
                                <BtnIcon style={{marginRight:'3px'}} className='fa fa-book'/> 
                                Check out
                            </CheckOutBtn>
                            <ItemModal id='item-modal'>
                               <ModalContent>
                                    <ItemHead>
                                        <HeadImage src={'https://res.cloudinary.com/bootable/image/upload/v1537700629/Daniel_Osineye_rvvle1.jpg'}/>
                                        <UserName>Daniel Osineye</UserName>
                                        <UserInstitution>African Leadership University</UserInstitution>
                                    </ItemHead> 
                                    <ItemText>
                                        Ideas are never fully formed. You just need to keep working on them. When we started, it was like a joke, we were not sure how this would turn out. We only gave it a shot and it paid off.
                                    </ItemText>
                                    <PostImage src={tino}/>
                                    <PostReactions>
                                        <ReactionCool src={cool}/>
                                        <CoolValue>12</CoolValue>
                                        <ReactionBoring src={boring}/>
                                        <BoringValue>2</BoringValue>
                                        <ReactionSmart src={smart}/>
                                        <SmartValue>32</SmartValue>
                                    </PostReactions>
                                </ModalContent> 
                            </ItemModal>
                        </ProfileBody>
                        <ProfileBody>
                            <ItemInfo>Daniel thinks this is cool</ItemInfo>
                            <ItemHead>
                                <HeadImage src={'https://res.cloudinary.com/bootable/image/upload/v1537700629/Daniel_Osineye_rvvle1.jpg'}/>
                                <UserName>Daniel Osineye</UserName>
                                <UserInstitution>African Leadership University</UserInstitution>
                            </ItemHead> 
                            <ItemText>
                                Ideas are never fully formed. You just need to keep working on them. When we started, it was like a joke, we were not sure how this would turn out. We only gave it a shot and it paid off.
                            </ItemText>
                            <CheckOutBtn>
                                <BtnIcon style={{marginRight:'3px'}} className='fa fa-book'/> 
                                Check out
                            </CheckOutBtn>
                        </ProfileBody>
                        <ProfileBody>
                            <ItemInfo>Daniel thinks this is cool</ItemInfo>
                            <ItemHead>
                                <HeadImage src={'https://res.cloudinary.com/bootable/image/upload/v1537700629/Daniel_Osineye_rvvle1.jpg'}/>
                                <UserName>Daniel Osineye</UserName>
                                <UserInstitution>African Leadership University</UserInstitution>
                            </ItemHead> 
                            <ItemText>
                                Ideas are never fully formed. You just need to keep working on them. When we started, it was like a joke, we were not sure how this would turn out. We only gave it a shot and it paid off.
                            </ItemText>
                            <CheckOutBtn>
                                <BtnIcon style={{marginRight:'3px'}} className='fa fa-book'/> 
                                Check out
                            </CheckOutBtn>
                        </ProfileBody>
                        <ProfileBody>
                            <ItemInfo>Daniel thinks this is cool</ItemInfo>
                            <ItemHead>
                                <HeadImage src={'https://res.cloudinary.com/bootable/image/upload/v1537700629/Daniel_Osineye_rvvle1.jpg'}/>
                                <UserName>Daniel Osineye</UserName>
                                <UserInstitution>African Leadership University</UserInstitution>
                            </ItemHead> 
                            <ItemText>
                                Ideas are never fully formed. You just need to keep working on them. When we started, it was like a joke, we were not sure how this would turn out. We only gave it a shot and it paid off.
                            </ItemText>
                            <CheckOutBtn>
                                <BtnIcon style={{marginRight:'3px'}} className='fa fa-book'/> 
                                Check out
                            </CheckOutBtn>
                        </ProfileBody>
                    </ProfileWrap>
                </Wrapper>
            </Fragment>
        );
    }
}

export default Profile;
import React,{Component} from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import nerd from './icons/nerd-man.svg'

const Main = styled.div`
    margin-top:20px;
    margin-left: 10%;
    margin-right: 10%;
`
const Pagination = styled.div`
    background-color: #FFFFFF;
    height: 22px
    padding: 3px;
    align-content: center;
    color: black;
    text-align:left;
`
const Identity = styled.span`
    display:block-inline;
` 
const PageIcon = styled.img`
    height:20px;
    width:20px;
`
const Page = styled.span`
    position:relative;
    top:-0.5ex;
    right:-0.4ex;
    font-size:0.9em;
`
class Wrapper extends Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <Main>
                <Pagination>
                    <Identity>
                        <PageIcon src={nerd}/>
                        <Page>{this.props.page}</Page>
                    </Identity>
                </Pagination>
                {this.props.children}
            </Main>   
        )
    }
}

Wrapper.propTypes = {
    page:PropTypes.string.isRequired
}
export default Wrapper;
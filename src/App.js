import React, { Component } from 'react'
import styled from 'styled-components'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import DashboardC from './containers/DashboardC'
import Profile from './components/Profile/Profile'
import Signin from './components/Auth/Signin'
import Signup from './components/Auth/Signup'
import Signout from './components/Auth/Signout'
import './App.css'
import {Provider} from 'react-redux'
import store from './store'

const Outer = styled.div`
  text-align: center;
  width:100%;
`

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Outer>
              <Route exact path='/' component={DashboardC}/>
              <Route exact path='/signin' component={Signin}/>
              <Route exact path='/signup' component={Signup}/>
              <Route exact path='/signout' component={Signout}/>
              <Route exact path='/profile' component={Profile}/>
            </Outer>
          </Switch>
        </Router>
      </Provider>
    );
  }
}

export default App;

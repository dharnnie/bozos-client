import {createStore, applyMiddleware,compose} from 'redux'
import thunk from 'redux-thunk'
import rootReducer from './redux/reducers'
const initialState = {}

const middleware = [thunk]

const composeEnhacers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
    rootReducer,
    initialState,
    composeEnhacers(
        applyMiddleware(...middleware)
    )
);

export default store;
import React,{Component} from 'react'
import Dashboard from '../components/Dashboard/Dashboard'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'
import {fetchPosts,
    fetchWordOfTheDay,
    createPost,
    updateReactionLocally,
    updateServerReaction,
    postNewComment,
    mutateReactorLocally,
    fetchComments
} from '../redux/actions/feedActions';

class DashboardC extends  Component{
    constructor(props){
        super(props)
        this.cp = this.cp.bind(this)
        this.updateRxnLocally = this.updateRxnLocally.bind(this)
        this.updateServerRxn = this.updateServerRxn.bind(this)
        this.postComment = this.postComment.bind(this)
        this.mutateRxtorLocally = this.mutateRxtorLocally.bind(this)
        this.mutateServerRxtor = this.mutateServerRxtor.bind(this)
    }
    componentWillMount(){
        let token = localStorage.getItem("token")
        this.props.fetchPosts(token);
        this.props.fetchWordOfTheDay();
        this.props.fetchComments(token)
        document.title = 'BOZOS | Better Millenials'
    }
    cp(data){
        let token = localStorage.getItem("token")
        this.props.createPost(token,data);
    }
    updateRxnLocally(data){
        this.props.updateReactionLocally(data)
    }
    mutateRxtorLocally(data){
        this.props.mutateReactorLocally(data)
    }
    updateServerRxn(data){
        let token = localStorage.getItem("token")
        this.props.updateServerReaction(token,data)
    }
    mutateServerRxtor(data){
        let token = localStorage.getItem("token")
        this.props.mutateServerReactor(token,data)
    }
    postComment(data){
        let token = localStorage.getItem("token")
        this.props.postNewComment(token,data)
    }
    render(){
        if (!this.props.signedIn){
            return(
                <Redirect to='/signout'/>
            )
        }
        return(
            <Dashboard 
                posts={this.props.posts} 
                reactions={this.props.reactions}
                postIds={this.props.postIds}
                reactors={this.props.reactors}
                wodd={this.props.wodd} 
                newPostRes={this.props.newPostRes}
                cp={this.cp}
                updateRxnLocally={this.updateRxnLocally}  
                updateServerRxn={this.updateServerRxn}
                postComment={this.postComment}
                mutateRxtorLocally={this.mutateRxtorLocally}
                mutateServerRxtor={this.mutateServerRxtor}
            />
        )
    }
}

DashboardC.propTypes = {
    fetchPosts: PropTypes.func.isRequired,
    fetchWordOfTheDay: PropTypes.func.isRequired,
    posts: PropTypes.array.isRequired,
    reactions: PropTypes.array.isRequired,
    reactors: PropTypes.array.isRequired,
    wodd: PropTypes.object.isRequired,
}
const mapStateToProps = state => ({
    posts: state.feed.posts,
    reactions: state.feed.reactions,
    postIds: state.feed.postIds,
    reactors: state.feed.reactors,
    wodd: state.feed.wodd,
    signedIn: state.auth.signedIn,
    newPostRes: state.feed.newPostRes
})

export default connect(mapStateToProps,{
        fetchPosts,
        fetchWordOfTheDay,
        createPost,
        updateReactionLocally,
        updateServerReaction,
        postNewComment,
        mutateReactorLocally,
        fetchComments
    })(DashboardC);
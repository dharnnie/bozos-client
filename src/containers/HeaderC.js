import React,{Component} from 'react'
import Header from '../components/Header/Header'
import PropTypes from 'prop-types'
import {connect} from 'react-redux';
import {fetchNotifications} from '../redux/actions/hActions';

class HeaderC extends  Component{
    constructor(props){
        super(props)
    }
    componentWillMount(){
        let token = localStorage.getItem("token")
        this.props.fetchNotifications(token)
    }
    render(){
        return(
            <Header 
                notifications={this.props.notifications}
            />
        )
    }
}

HeaderC.propTypes = {
    notifications: PropTypes.array.isRequired,
}
const mapStateToProps = state => ({
    notifications: state.header.notifications,
})

export default connect(mapStateToProps,{
        fetchNotifications
    })(HeaderC);
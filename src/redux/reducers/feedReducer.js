import {FETCH_POSTS, 
    FETCH_WODD,
    CREATE_POST, 
    UPDATE_REACTION_LOCALLY, 
    POST_NEW_COMMENT,
    MUTATE_REACTOR_LOCALLY,
    MUTATE_SERVER_REACTOR,
    FETCH_COMMENTS,
    FETCH_COMMENTS_BY_PID
} from '../actions/actions'
import {COOL, BORING, SMART} from '../reducers/reactions'
 
const initialState = {
    posts:[],
    reactions:[],
    postIds:[],
    wodd:{},
    newPostRes:'error',
    notifications:[],
    comments:[],
    reactors:[]
}

export default function (state = initialState, action){
    switch(action.type){
        case FETCH_POSTS:
            let data = normalizePosts(action.payload)
            return{
                ...state,
                posts: data.posts.reverse(),
                reactions: data.reactions.reverse(),
                postIds: data.ids.reverse(),
                reactors:data.reactors.reverse()
            }
        case FETCH_WODD:
            return{
                ...state,
                wodd: action.payload
            }
        case CREATE_POST:
            return{
                ...state
            }
            //let newData = normalizeNewestPost(state, action.payload.post)
            // return{
            //     ...state,
            //     newPostRes: action.payload.status,
            //     posts: newData.posts,
            //     reactions: newData.reactions,
            //     postIds: newData.ids,
            //     reactors: newData.reactors
            // }
        case UPDATE_REACTION_LOCALLY:
            return{
                ...state,
                reactions: updatedReactions(state, action)
            }
        case POST_NEW_COMMENT:
            return{
                ...state,
                comments: updateComments(state,action)
            }
        case MUTATE_REACTOR_LOCALLY:
            return{
                ...state,
                reactors: updateReactors(state,action)
            }
        case FETCH_COMMENTS:
            let c = []
            if (Array.isArray(action.payload)){
                return{
                    ...state,
                    comments: action.payload
                }
            }else{
                return{
                    ...state,
                    comments: c
                }
            }
        case FETCH_COMMENTS_BY_PID:
            return{
                ...state,
                comments: updateComments(state, action)
            }
        default:
            return state;
    }
}

// returns a new array of reactions with the updated state
function updatedReactions(state, action){
    let ob = updateSingleRxn(state,action)
    return ob 
}

function updateSingleRxn(state, action){
    const {payload} = action
    const {id, rxn,rxn2,stat} = payload
    console.log('Id from payload is - ', id)
    let index = 0
    let objectToUse = state.reactions.find((v, i) => {
        if(v.allIds[i] === id){
            index = i
            return v
        }
    })
    console.log("Index of reactions ", index)
    let o = JSON.parse(JSON.stringify(objectToUse))
    let r = updateSingleRxnObject(o,rxn,rxn2, index,stat)
    let copyOfAllReactions = JSON.parse(JSON.stringify(state.reactions))
    copyOfAllReactions[index] = r
    return copyOfAllReactions
}

function updateSingleRxnObject(o, rxn, rxn2,index,stat){
    let objId = o.allIds[index]
    console.log(objId)
    if(stat){
        switch(rxn){
            case COOL:
                o[objId].cool++
                break
            case BORING:
                o[objId].boring++
                break
            case SMART:
                o[objId].smart++
                break
        }
    }else{
        if (rxn2 !== rxn){
            switch(rxn2){
                case COOL:
                    o[objId].cool--
                    break
                case BORING:
                    o[objId].boring--
                    break
                case SMART:
                    o[objId].smart--
                    break
            }
        }else{
            switch(rxn){
                case COOL:
                    o[objId].cool--
                    break
                case BORING:
                    o[objId].boring--
                    break
                case SMART:
                    o[objId].smart--
                    break
            }
        }
    }
    return o
}


function normalizePosts(posts){
    var allNormedPosts = []
    var allNormedRxns = []
    var postIds = []
    var allNormedRxtors = []
    var normalized = {}
    for(let post of posts){
        var normedPost = {
            [post.id]:{
                id: post.id,
                nick: post.nick,
                firstname: post.firstname,
                lastname: post.lastname,
                avatar: post.avatar,
                institution:post.institution,
                text: post.text,
                posttype: post.posttype,
                time: post.time,
            }
        }
        var normedRxns = {
            [post.id]:{
                id: post.id,
                cool: post.reactions.cool,
                boring: post.reactions.boring,
                smart: post.reactions.smart
            }
        }
        var normedRxtors = {
            [post.id]:{
                id:post.id,
                rxtors: post.reactors.rxtors,
                rxtorsIds:stripEachPostRxtorsIds(post.reactors.rxtors)
            }
        }
        postIds.push(post.id)
        normedPost.allIds = postIds
        normedRxns.allIds = postIds
        normedRxtors.allIds = postIds
        allNormedPosts.push(normedPost)
        allNormedRxns.push(normedRxns)
        allNormedRxns.allIds = postIds
        allNormedRxtors.push(normedRxtors)
    }
    normalized = {
        posts: allNormedPosts,
        reactions: cleanUpReactions(allNormedRxns),
        ids: postIds,
        reactors: allNormedRxtors
    }

    return normalized
}

// function normalizeNewestPost(state, post){
    
// }

function stripEachPostRxtorsIds(r){
    let rxtorsIds = []
    r.forEach((item)=>{
        rxtorsIds.push(item.id)
    })
    return rxtorsIds
}

function cleanUpReactions(r){
    let newArr = []
    r.forEach((item,i) => {
        let theId = r.allIds[i]
        item[theId].cool = item[theId].cool === undefined ? 0 : item[theId].cool
        item[theId].boring = item[theId].boring === undefined ? 0 : item[theId].boring
        item[theId].smart = item[theId].smart === undefined ? 0 : item[theId].smart
        newArr.push(item)
    })
    return newArr
}

function updateComments(state, action){
    let copyOfComments = JSON.parse(JSON.stringify(state.comments))
    copyOfComments.push(action.payload)
    return copyOfComments
}

function updateReactors(state,action){
    const {payload} = action
    const {pid, uid, rxn, add} = payload
    var index = 0
    let copyOfReactors = JSON.parse(JSON.stringify(state.reactors))
    let objectToUse = copyOfReactors.find((v, i) => {
        index = i
        return v.allIds[i] === pid
    })
    console.log(objectToUse)
    let objId = objectToUse.allIds[index]
    console.log('Important index', index)
    console.log('User Id is', uid   )
    if(add){
        // Change the payload uid to the new object type
        console.log('We are adding')
        objectToUse[objId].rxtors.push({id:uid,rxn:rxn})
        objectToUse[objId].rxtorsIds.push(uid) // push new id to rxtorsId
        copyOfReactors[index] = objectToUse
        console.log('After adding', copyOfReactors)
        return copyOfReactors
    }else{
        // remove the reactor object from the list
        console.log('We are removing')
        // get the rxtor of  a post by uid
        let indexToRemove = 0
        let shit = objectToUse[objId].rxtors.find((v,i) => {
            indexToRemove = i
            return v.id === uid
        })
        //let indexToRemove = objectToUse[objId].rxtorsIds[index]
        console.log("Index to remove",indexToRemove)
        let removed = objectToUse[objId].rxtors.splice(indexToRemove,1) 
        objectToUse[objId].rxtorsIds.splice(indexToRemove,1)
        copyOfReactors[index] = objectToUse
        console.log('After removing',copyOfReactors)
        console.log('Removed is ', removed)
        return copyOfReactors
    }
}
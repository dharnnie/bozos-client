import {FETCH_NOTIFICATIONS} from '../actions/actions'
 
const initialState = {
    notifications:[]
}

export default function (state = initialState, action){
    switch(action.type){
        case FETCH_NOTIFICATIONS:
            if(action.payload === null){
                return{
                    ...state
                }
            }else{
                return{
                    ...state,
                    notifications: action.payload
                }
            }
            
        default:
            return state;
    }
}
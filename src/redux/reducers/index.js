import {combineReducers} from 'redux'
import feedReducer from './feedReducer'
import authReducer from './authReducer';
import hReducer from './hReducer'

export default combineReducers({
    feed: feedReducer,
    auth:authReducer,
    header: hReducer
})
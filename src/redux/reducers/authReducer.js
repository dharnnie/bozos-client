import {SIGN_IN, SIGN_UP, EMAIL_EXISTS, SET_SIGNED_IN} from '../actions/actions'

const initialState = {
    signedIn: localStorage.getItem('signedIn'),
    signInRes:{},
    signUpRes:{},
    emailExists:false,
    nickExists: false
}

export default function(state = initialState, action){
    switch(action.type){
        case SIGN_IN:
            return{
                ...state,
                signInRes:action.payload
            }
        case SIGN_UP:
            return{
                ...state,
                signUpRes: action.payload
            }
        case EMAIL_EXISTS:
            return{
                ...state,
                emailExists: action.payload.response
            }
        case EMAIL_EXISTS:
            return{
                ...state,
                nickExists: action.payload.response
            }
        case SET_SIGNED_IN:
            return{
                ...state,
                signedIn: action.payload
            }
        default:
            return{
                ...state
            }
    }
}       


import {FETCH_NOTIFICATIONS} from './actions'
import {appConfig} from '../../config'

const URL = appConfig.CURRENT_API_URL
const config = {
    headers:{
        'Content-Type':'application/json'
    }
}
export const fetchNotifications = (token) => dispatch =>{
    fetch(`${URL}/notifications`,{
        headers:{
            'Content-Type':'application/json',
            'Authorization':token,
        }
    })
    .then(res => res.json())
    .then(notifs => dispatch({
        type: FETCH_NOTIFICATIONS,
        payload:notifs
    }))
}
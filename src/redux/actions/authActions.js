import {SIGN_IN,SIGN_UP, NICK_EXISTS, EMAIL_EXISTS, SET_SIGNED_IN} from './actions'
import axios from 'axios'
import {appConfig} from '../../config'

const config = {
    headers:{
        'Content-Type':'application/json'
    }
}
const URL = appConfig.CURRENT_API_URL

export const signUp = (data) =>async dispatch => {
    const res = await fetch(`${URL}/signup`,{
        headers:config.headers,
        method:'POST',
        body:JSON.stringify(data)
    })
    const payload = await res.json();
    dispatch({
        type:SIGN_UP,
        payload:payload
    })
}
export const signIn = (data) => async dispatch => {
    const res = await fetch(`${URL}/auth`,{
        headers:config.headers,
        method:'POST',
        body: JSON.stringify(data)
    })
    const payload = await res.json()
    dispatch({
        type:SIGN_IN,
        payload:payload
    })
}

export const eExists = (email) => dispatch => {
    axios.get(`${URL}/exists/${email}`,config)
    .then(res => dispatch({
        type: EMAIL_EXISTS,
        payload: res.data
    }))
}
export const nExists = (nick) => dispatch => {
    axios.get(`${URL}/exists/${nick}`,config)
    .then(res => dispatch({
        type: NICK_EXISTS,
        payload: res.data
    }))
}

export const setSignedIn = (value) => dispatch => {
    dispatch({
        type: SET_SIGNED_IN,
        payload: value
    })
}
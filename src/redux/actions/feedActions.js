import {FETCH_POSTS, 
    FETCH_WODD, 
    CREATE_POST, 
    UPDATE_REACTION_LOCALLY, 
    UPDATE_SERVER_REACTION, 
    CREATE_NEW_COMMENT, 
    POST_NEW_COMMENT,
    MUTATE_REACTOR_LOCALLY,
    MUTATE_SERVER_REACTOR,
    FETCH_COMMENTS,
    FETCH_COMMENTS_BY_PID
} from './actions'
import {appConfig} from '../../config'

const URL = appConfig.CURRENT_API_URL
const config = {
    headers:{
        'Content-Type':'application/json'
    }
}
export const fetchPosts = (token) => dispatch =>{
    fetch(`${URL}/getposts`,{
        headers:{
            'Content-Type':'application/json',
            'Authorization':token,
        }
    })
    .then(res => res.json())
    .then(posts => dispatch({
        type: FETCH_POSTS,
        payload:posts
    }))
}

export const fetchWordOfTheDay = () => dispatch => {
    fetch(`${URL}/wodd`,{
        headers:{
            'Content-Type':'application/json'
        }
    })
    .then(res => res.json())
    .then(wodd => dispatch({
        type: FETCH_WODD,
        payload:wodd
    }))
}
export const createPost = (token,data) =>async dispatch =>{
    const res = await fetch(`${URL}/newpost`,{
        headers:{
            'Content-Type':'application/json',
            'Authorization':token,
        },
        method:'POST',
        body:JSON.stringify(data)
    })
    const payload = await res.json();
    dispatch({
        type:CREATE_POST,
        payload:payload
    })
}

export const fetchComments = (token) => async dispatch =>{
    const res = await fetch(`${URL}/getcomments`,{
        headers:{
            'Content-Type':'application/json',
            'Authorization':token,
        }
    })
    const payload = await res.json();
    dispatch({
        type:FETCH_COMMENTS,
        payload:payload
    })
} 
export const fetchCommentsById = (token,pid) => async dispatch =>{
    const res = await fetch(`${URL}/getcommentsbyid/${pid}`,{
        headers:{
            'Content-Type':'application/json',
            'Authorization':token,
        }
    })
    const payload = await res.json();
    dispatch({
        type:FETCH_COMMENTS_BY_PID,
        payload:payload
    })
} 
export const updateServerReaction = (token,data) =>async dispatch =>{
    const res = await fetch(`${URL}/newreaction`,{
        headers:{
            'Content-Type':'application/json',
            'Authorization':token,
        },
        method:'POST',
        body:JSON.stringify(data)
    })
    const payload = await res.json();
    console.log(payload)
    dispatch({
        type:UPDATE_SERVER_REACTION,
        payload:payload
    })
}

export const postNewComment = (token,data) => async dispatch =>{
    alert('this is postNewComment - feedActions')
    const res = await fetch(`${URL}/newcomment`,{
        headers:{
            'Content-Type':'application/json',
            'Authorization':token,
        },
        method:'POST',
        body:JSON.stringify(data)
    })
    const payload = await res.json();
    console.log(payload)
    dispatch({
        type:POST_NEW_COMMENT,
        payload:payload
    })
}

export const updateReactionLocally = (data) => dispatch =>{
    dispatch({
        type: UPDATE_REACTION_LOCALLY,
        payload: data
    })
}

export const mutateReactorLocally = (data) => dispatch =>{
    dispatch({
        type: MUTATE_REACTOR_LOCALLY,
        payload: data
    })
}